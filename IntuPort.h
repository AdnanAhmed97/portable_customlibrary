
#ifndef IntuginePort_h
#define IntuginePort_h
#include <SoftwareSerial.h>
#include "Arduino.h"


#define DEFAULT_RX_PIN 		3
#define DEFAULT_TX_PIN 		5
#define DEFAULT_RESET_PIN 	2		// pin to the reset pin IntuginePort

#define DEFAULT_LED_FLAG	true 	// true: use led.	 false: don't user led.
#define DEFAULT_LED_PIN 	13 		// pin to indicate states.

#define BUFFER_RESERVE_MEMORY	1024
#define DEFAULT_BAUD_RATE		9600
#define TIME_OUT_READ_SERIAL	5000


class IntuginePort : public SoftwareSerial
{
private:

    uint32_t _baud;
    int _timeout;
    String _buffer;
    bool _sleepMode;
    uint8_t _functionalityMode;
    String _locationCode;
    String _longitude;
    String _latitude;

    String _readSerial();
    String _readSerial(uint32_t timeout);


public:

    uint8_t	RX_PIN;
    uint8_t TX_PIN;
    uint8_t RESET_PIN;
    uint8_t LED_PIN;
    bool	LED_FLAG;

    IntuginePort(void);
    IntuginePort(uint8_t rx, uint8_t tx);
    IntuginePort(uint8_t rx, uint8_t tx, uint8_t rst);
    IntuginePort(uint8_t rx, uint8_t tx, uint8_t rst, uint8_t led);

    void begin();					//Default baud 9600
    void begin(uint32_t baud);
    void reset();

    bool setSleepMode(bool state);
    bool getSleepMode();
    bool setFunctionalityMode(uint8_t fun);
    uint8_t getFunctionalityMode();

    bool setPIN(String pin);
    String getProductInfo();

    String getOperatorsList();
    String getOperator();

    bool calculateLocation();
    String getLocationCode();
    String getLongitude();
    String getLatitude();

    bool answerCall();
    void callNumber(char* number);
    bool hangoffCall();
    uint8_t getCallStatus();

    bool sendSms(char* number,char* text);
    String readSms(uint8_t index);
    String getNumberSms(uint8_t index);
    bool delAllSms();


    String signalQuality();
    void setPhoneFunctionality();
    void activateBearerProfile();
    void deactivateBearerProfile();

    void RTCtime(int *day,int *month, int *year,int *hour,int *minute, int *second);
    String dateNet();
    bool updateRtc(int utc);
    long readBuffer(char* buffer,int count,const char* resp, unsigned int timeOut);

    
    void cleanBuffer(char* buffer, int count);
    
  
    void sendCmd(const char* cmd);

    int sendATTest(void);
  
    void sendEndMark(void);
  

    int waitForResp(const char* resp, unsigned timeout);

    int sendCmdAndWaitForResp(const char* cmd, const char *resp, unsigned timeout);

};

#endif
